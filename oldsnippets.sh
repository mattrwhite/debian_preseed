# ion3 no longer avail on debian - use notion instead
# rxvt-beta no longer available (rxvt has a terminal size bug under ion3)
# - for reference, use /usr/bin/rxvt-xpm with rxvt-beta

# since rxvt doesn't support unicode, get rid of utf-8
# LANG and LC_ALL must be set to en_US in .bashrc
# for this to work, you must uncomment "en_US ISO-8859-1" in /etc/locale.gen
##locale-gen en_US


# install mercurial crecord extension
#mkdir .hgext &&
#hg clone --insecure https://bitbucket.org/edgimar/crecord/ .hgext/crtmp &&
#mv .hgext/crtmp/crecord .hgext/crecord &&
#rm -r .hgext/crtmp &&
#echo "Installed hg crecord extension"

# fetch and install themes/wincursors.tar.gz
#  doesn't take effect until config repo is installed
#mkdir /tmp/wincursors
#cd /tmp/wincursors
#wget $SERVER/home/debinst/themes/wincursors.tar.gz
#tar -xzf wincursors.tar.gz
#make -C ./sources install