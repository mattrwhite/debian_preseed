#!/bin/sh
# installation script for user - don't run as root!
GATEWAY=`ip addr | sed -nE 's/^\s*inet ([0-9]+\.[0-9]+\.[0-9]+).*eth0$/\1.1/p'`
SERVER="http://$GATEWAY:8080"

# setup ssh for incoming and outgoing - don't want to put these keys into my ion3cfg repo obviously
mkdir $HOME/.ssh &&
wget -P $HOME/.ssh $SERVER/misc/ssh/id_rsa &&
wget -P $HOME/.ssh $SERVER/misc/ssh/id_rsa.pub &&
wget -P $HOME/.ssh $SERVER/misc/ssh/authorized_keys &&

# user specific config - use clone instead of previous init + pull so that default path gets set
hg clone --insecure https://bitbucket.org/mattrwhite/ion3cfg ion3cfg &&
mv ion3cfg/.hg $HOME &&
hg -R $HOME update -C &&
rm -r ion3cfg &&
echo "Installed user configuration repo"

# Dina fonts not useful for high DPI display
# fetch Dina fonts and copy to /home/mwhite/.fonts, then create/update fonts.dir file
#wget -P /tmp $SERVER/themes/Dina-BDF.tar.gz &&
#mkdir -p $HOME/.fonts &&
#tar -C $HOME/.fonts -xzf /tmp/Dina-BDF.tar.gz &&
#mkfontdir $HOME/.fonts &&
#rm /tmp/Dina-BDF* &&
#echo "Installed Dina font"
