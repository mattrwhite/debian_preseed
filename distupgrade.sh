#!/bin/sh
# as root, run `wget http://192.168.x.x:8080/preseed/preseed/distupgrade.sh | sh`
# TODO: try http://mirrors.kernel.org/debian first (much faster, but broken last time)

# replace old sources.list
echo 'deb http://http.debian.net/debian unstable main contrib non-free' > /etc/apt/sources.list
echo 'deb-src http://http.debian.net/debian unstable main contrib non-free' >> /etc/apt/sources.list
aptitude update && aptitude dist-upgrade