#!/bin/sh
# Instructions:
# 1. install debian stable from netinst ISO (~250 MB) with network disconnected
#  - set network to host-only, but leave adapter connected (disconnecting adapter yields invalid network config)
#  - press escape to get past invalid mirror error
#  - there is no corresponding iso for unstable
#  - apt-pinning is an alternative to unstable: http://jaqque.sbih.org/kplug/apt-pinning.html
#  - set networking back to NAT before rebooting
# 2. run mongoose on host, login as root and run `curl http://192.168.x.x:8080/preseed/distupgrade.sh | sh`
#  - might need to `aptitude remove` old linux-image
#  - take a snapshot
# 3. run `curl http://192.168.x.x:8080/preseed/postinst.sh | sh`
# 4. login as mwhite, run `curl http://192.168.x.x:8080/preseed/userinst.sh | sh`
# 5. run `startx`, verify clipboard functionality, shutdown, and take a snapshot


# abort script on error
set -e

# command for installing packages - apt-get seems to have leapfrogged aptitude
#APTINST="aptitude --without-recommends -y install "
APTINST="apt-get --no-install-recommends -y install "
# local server - VMware gateway IP ends with .2, but mongoose is only available on IP ending with .1
GATEWAY=`ip addr | sed -nE 's/^\s*inet ([0-9]+\.[0-9]+\.[0-9]+).*eth0$/\1.1/p'`
SERVER="http://$GATEWAY:8080"

# not included in debian minimal...
$APTINST sudo xorg build-essential gdb file fontconfig

# notion window manager (fork of ion3); rxvt-unicode terminal emulator; scalable cursor theme
$APTINST notion rxvt-unicode-256color dmz-cursor-theme
# make rxvt the default terminal emulator
update-alternatives --set x-terminal-emulator /usr/bin/urxvt
# use urxvtcd (not available through update-alternatives) for reduced memory usage
##ln -s /usr/bin/urxvtcd /usr/bin/x-terminal-emulator
# default is DMZ-White
update-alternatives --set x-cursor-theme /usr/share/icons/DMZ-Black/cursor.theme

# mc for file management, hex editor, quick view; apt-file for searching for
#  package which provides a given command; lsof is a handy util
# silversearcher-ag is a faster version of ack-grep
# htop is a mouse-enabled replacement for top
# feh is an image viewer, lightweight compared to imagemagick
# ncdu is a better du
# rlwrap provides readline support (history, completion) for any command
# xsel for command line clipboard access
$APTINST mc apt-file less lsof silversearcher-ag htop feh ncdu rlwrap rsync unzip xsel
# this package will call apt-file automatically on bash command_not_found
##$APTINST command-not-found

# basic packages; mercurial depends on ca-certificates
$APTINST mercurial git vim lua5.3 cifs-utils openssh-server ca-certificates
# viewglob is rarely used and too big; evince for viewing pdf, djvu - maybe djvulibre instead?
# Note: msttcorefonts prompts
##$APTINST xbindkeys conky evince msttcorefonts viewglob
# finding fastest apt server:
##$APTINST netselect-apt
##netselect-apt unstable
# hunspell for my scite spell checker, libgtk needed for scite
$APTINST hunspell libgtk2.0-0

# don't expect to see me install linux outside a vm anytime soon...
# open-vm-toolbox no longer appears to be needed unless you need GUI version
# - cut and paste broken with debian unstable Feb 2014 - have to use vmware tools + https://github.com/rasa/vmware-tools-patches/
$APTINST xserver-xorg-input-vmmouse xserver-xorg-video-vmware open-vm-tools open-vm-tools-desktop open-vm-tools-dkms

# sudo group almost certainly exists already
addgroup sudo
adduser mwhite sudo
echo "%sudo ALL=NOPASSWD: ALL" > /etc/sudoers.d/nopasswd && chmod 440 /etc/sudoers.d/nopasswd

# prevent this code from running twice
if [ ! -e "/root/.vimrc" ]
then

# use files from mwhite for bash and vim for root
# symlinks can be created even if targets (first arg) don't exist yet
ln -fs /home/mwhite/.bashrc /root/.bashrc
ln -fs /home/mwhite/.inputrc /root/.inputrc
ln -fs /home/mwhite/.vimrc /root/.vimrc
ln -fs /home/mwhite/.vim /root/.vim

# Mounting Windows shares: I mostly use rsync to copy between VM and host now
##$APTINST cifs-utils
# User must manually run `sudo mount -a` and enter Windows password
##echo "//$GATEWAY/home /home/mwhite/home cifs mapchars,user=Administrator,uid=mwhite,gid=mwhite" >> /etc/fstab

# prevent `/var/cache/apt/archives` from getting too big; this works via `/etc/cron.daily/apt`
echo 'APT::Archives::MaxAge "5";
APT::Archives::MaxSize "100";' >> /etc/apt/apt.conf.d/20archive

# install vimcat for cat with syntax highlighting
wget http://www.vim.org/scripts/download_script.php?src_id=23422 -O /usr/local/bin/vimcat && chmod 755 /usr/local/bin/vimcat

# install fonts
# to get proper size in Qt apps, add `font="Tahoma,8,-1,5,50,0,0,0,0,0"` to [Qt] section in ~/config/Trolltech.conf
[ ! -f /usr/share/fonts/truetype/tahoma.ttf -o ! -f /usr/share/fonts/truetype/tahomabd.ttf ] &&
mkdir -p /usr/share/fonts/truetype/ &&
wget -P /usr/share/fonts/truetype $SERVER/themes/tahoma.ttf &&
wget -P /usr/share/fonts/truetype $SERVER/themes/tahomabd.ttf &&
wget -P /usr/share/fonts/truetype $SERVER/themes/Menlo-Regular.ttf &&
chmod 644 /usr/share/fonts/truetype/*.ttf &&
fc-cache -v &&
echo "Installed fonts"

# install scite
SCITE=/usr/share/scite

install_scite_bin() {
  if [ $(uname -m) = 'x86_64' ]; then
    wget -O /usr/bin/scite $SERVER/misc/linux64/SciTE-3.7.0 &&
    chmod 755 /usr/bin/scite &&
    wget -P $SCITE $SERVER/misc/linux64/hunspell.so &&
    wget -P $SCITE $SERVER/misc/linux64/unix-spawner-ex.so
  else
    mv /tmp/gscite/SciTE /usr/bin/scite &&
    wget -P $SCITE $SERVER/misc/linux32/hunspell.so &&
    wget -P $SCITE $SERVER/misc/linux32/unix-spawner-ex.so
  fi
}

wget -P /tmp http://www.scintilla.org/gscite370.tgz
tar -C /tmp -xaf /tmp/gscite370.tgz
hg clone --insecure https://bitbucket.org/mattrwhite/scite_lua $SCITE
mkdir -p $SCITE/syntax/
mv /tmp/gscite/SciTE*.properties $SCITE
mv /tmp/gscite/*.properties $SCITE/syntax/
install_scite_bin
mv /tmp/gscite/* $SCITE
sed -i 's/^import \*/import SciTECustom/g' $SCITE/SciTEGlobal.properties
rm -r /tmp/gscite*
echo "Installed SciTE"

# update apt-file cache
apt-file update

# clear aptitude cache
aptitude clean

fi
